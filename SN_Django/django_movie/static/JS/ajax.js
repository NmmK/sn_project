function ajaxSend(url, params) {
    // Отправляем запрос
    fetch(`${url}?${params}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
    })
        .then(response => response.json())
        .then(json => render(json))
        .catch(error => console.error(error))
}

// Поиск формы по имени
const forms = document.querySelector('form[name=filter]');

//forms.addEventListener('submit', function (e) {
//    // Получаем данные из формы
////    предотвращаем отправку формы и след перезагрузку страницы
//    e.preventDefault();
////    заносим информацию из атрибута action в переменную
//    let url = this.action;
////    передача формы(this)
//    let params = new URLSearchParams(new FormData(this)).toString();
////    параметры - это жанры, годы и тд
//    ajaxSend(url, params);
//});

function render(data) {
    // Рендер шаблона
    let template = Hogan.compile(html);
    let output = template.render(data);

    const div = document.querySelector('.movie_list');
    div.innerHTML = output;
}

let html = '\
{{#movies}}\
           <div class="movie_item">\
             <div class="poster">\
               <img src="media/{{ poster }}">\
             </div>\
             <div class="info">\
               <div class="title">\
                 <h2>\
                   <a href="/{{ url }}">{{ title }}</a>\
                 </h2>\
                </div>\
               <div class="detail_info">\
                 <ul class="info_list">\
                   <li>Жанр: {{ genres__name }}</li>\
                   <li>Продолжительность: {{ runtime }} мин.</li>\
                   <li>Год: {{ year }}</li>\
                 </ul>\
                 <div class="rating">Средняя Оценка 8.2/10</div>\
               </div>\
             </div>\
             <div class="buttons"><button class="add-to-list">Добавить в список</button></div>\
           </div>\
{{/movies}}'



// добавление рейтинга фильма
const rating = document.querySelector('form[name=rating]')

rating.addEventListener("change", function() {
    let data = new FormData(this);
    fetch(`${this.action}`, {method: 'POST', body: data})
        .then(response => alert("Рейтинг установлен"))
        .catch(error => alert("Ошибка"))
} );
















