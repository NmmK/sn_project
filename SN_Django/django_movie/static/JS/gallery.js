// ____________GALLERY____________
var modal = document.getElementById("modalPic"),
    picOpen = document.getElementById("clickPic"),
    spanClose = document.getElementsByClassName("btn_close")[0],
    img = document.querySelector(".modal_pic");
    images = document.getElementsByClassName("film_pic");

    
for (let i = 0; i < images.length; i++)
{
    images[i].addEventListener("click", function()
    {
        modal.style.display = "block";
        img.src = images[i].src;
    });
}

spanClose.addEventListener("click", function() {
  modal.style.display = "none";
});


window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

window.addEventListener("keydown", function(evt){
  if ((evt.key === "Escape") && (modal.style.display === "block")) {
    evt.preventDefault();
    modal.style.display = "none";
  }
})


for (let i = 0; i < modal.childNodes.length; i++)
{
    modal.childNodes[i].addEventListener("click", function(e)
    {
        e.stopImmediatePropagation();
    });
}

