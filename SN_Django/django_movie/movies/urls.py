from django.urls import path

from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path("", views.MovieView.as_view(), name="list_movie"),
    path("filter/", views.FilterMoviesView.as_view(), name='filter'),
    path("search/", views.Search.as_view(), name="search"),
    path("add-rating/", views.AddStarRating.as_view(), name='add_rating'),
    path("json-filter/", views.JsonFilterMoviesView.as_view(), name='json_filter'),
    path("<slug:slug>/", views.MovieDetailView.as_view(), name="detail_movie"),
    path("review/<int:pk>/", views.AddReview.as_view(), name="add_review"),
    path("<slug:slug>/gallery/", views.MovieGallery.as_view(), name="gallery"),
    path("actor/<str:slug>/", views.ActorView.as_view(), name="actor_detail")
]
