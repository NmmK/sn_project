from django import template
from movies.models import Category
from movies.models import Reviews

# регистрация template тегов
register = template.Library()


# регистрация функции как template simple тега
@register.simple_tag()
def get_categories():
    return Category.objects.all()


# регистрация template inclusion tag
@register.inclusion_tag('movies/tags/last_review.html')
def get_last_reviews(count=2):
    reviews = Reviews.objects.order_by("id")[Reviews.objects.count()-count:]
    return {"last_reviews": reviews}
