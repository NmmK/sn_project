from django.db.models import Q
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render


# Create your views here.
from django.shortcuts import render, redirect
from django.views.generic.base import View
from django.views.generic import ListView, DetailView

from movies.models import Movie, Actor, Genre, Rating
from .forms import ReviewForm, RatingForm


class GenreYearRating:
    """"""

    def get_genres(self):
        return Genre.objects.all()

    def get_years(self):
        return Movie.objects.filter(draft=False).values("year")


class MovieView(GenreYearRating, ListView):
    """Список фильмов"""
    # # Метод принимает get запросы
    # def get(self, request):
    #     movies = Movie.objects.all()
    #     return render(request, "movies/list_movie.html", {"movie_list": movies})
    model = Movie
    queryset = Movie.objects.filter(draft=False)
    template_name = "movies/list_movie.html"
    paginate_by = 1


class MovieDetailView(GenreYearRating, DetailView):
    """ Полное описание фильма """
    # def get(self, request, slug):
    #     movie = Movie.objects.get(url=slug)
    #     return render(request, "movies/movie_detail.html", {"movie": movie})
    model = Movie

    # по какому полю следует искать запись
    slug_field = "url"
    template_name = "movies/detail_movie.html"

    def get_context_data(self, **kwargs):
        # словарь
        context = super().get_context_data(**kwargs)
        # добавление ключа и присваивание ему значения
        context["star_form"] = RatingForm()
        context["form"] = ReviewForm()
        return context


class MovieGallery(GenreYearRating, DetailView):
    """ Полное описание фильма """
    # def get(self, request, slug):
    #     movie = Movie.objects.get(url=slug)
    #     return render(request, "movies/movie_detail.html", {"movie": movie})
    model = Movie
    slug_field = "url"
    template_name = "movies/gallery.html"


# Класс для отправки отзывов
class AddReview(View):
    """Отзывы"""

    def post(self, request, pk):
        # передача и заполнение данных из post запроса
        form = ReviewForm(request.POST)
        # делаем запрос в бд на получение объекта по id
        movie = Movie.objects.get(id=pk)
        # проверка формы на валидность
        if form.is_valid():
            # указываем к какому фильму привязать отзыв
            # сначало приостанавливаем сохр формы
            form = form.save(commit=False)
            # если в POST запросе есть ключ parent (name у тега input),
            # то есть это ответ на отзыв,
            if request.POST.get("parent", None):
                form.parent_id = int(request.POST.get("parent"))
            # обращаемся к полю movie(в модели Reviews это внешний ключ)
            # для завязки фильма и отзыва
            form.movie = movie
            form.save()
        return redirect(movie.get_absolute_url())


class ActorView(GenreYearRating, DetailView):
    model = Actor
    template_name = "movies/actor.html"
    slug_field = "name"


class FilterMoviesView(GenreYearRating, ListView):
    template_name = "movies/list_movie.html"
    paginate_by = 1

    def get_queryset(self):
        queryset = Movie.objects.filter(
            Q(year__in=self.request.GET.getlist("year")) |
            Q(genres__in=self.request.GET.getlist("genre"))
        ).distinct()
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["year"] = ''.join([f'year={x}&' for x in self.request.GET.getlist("year")])
        context["genre"] = ''.join([f'genre={x}&' for x in self.request.GET.getlist("genre")])
        return context


class JsonFilterMoviesView(ListView):
    template_name = "movies/list_movie.html"

    def get_queryset(self):
        queryset = Movie.objects.filter(
            Q(year__in=self.request.GET.getlist("year")) |
            Q(genres__in=self.request.GET.getlist("genre"))
        ).distinct().values("title", "poster", "runtime", "url", "year", "genres__name")
        print(queryset)
        return queryset

    def get(self, request, *args, **kwargs):
        queryset = list(self.get_queryset())
        return JsonResponse({"movies": queryset}, safe=False)


class AddStarRating(View):
    """"Добавление рейтинга к фильму"""

    def get_client_ip(self, request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = request.META.get('REMOTE_ADDR')
        return ip

    def post(self, request):
        form = RatingForm(request.POST)
        if form.is_valid():
            Rating.objects.update_or_create(
                ip=self.get_client_ip(request),
                movie_id=int(request.POST.get("movie")),
                defaults={'star_id': int(request.POST.get("star"))}
            )
            return HttpResponse(status=201)
        else:
            return HttpResponse(status=400)


class Search(ListView):
    """Поиск фильмов"""
    paginate_by = 1
    template_name = "movies/list_movie.html"

    def get_queryset(self):
        return Movie.objects.filter(title__contains=self.request.GET.get("q"))

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["q"] = f'q={self.request.GET.get("q")}&'
        return context
























