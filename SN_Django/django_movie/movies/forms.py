
from django import forms
from django.forms import RadioSelect
from snowpenguin.django.recaptcha3.fields import ReCaptchaField


from .models import Reviews, RatingStar, Rating

# форма на основе класса
class ReviewForm(forms.ModelForm):
    """Форма отзывов"""
    captcha = ReCaptchaField()


    class Meta:
        model = Reviews
        fields = ("name", "email", "text")
        widgets = {
            "name": forms.TextInput(attrs={"class": "form-control border", "placeholder": "Ваше имя"}),
            "email": forms.EmailInput(attrs={"class": "form-control border", "placeholder": "Ваш Email"}),
            "text": forms.Textarea(attrs={"class": "form-control border"})
        }


class RatingForm(forms.ModelForm):
    star = forms.ModelChoiceField(
        queryset=RatingStar.objects.all(), widget=forms.RadioSelect(), empty_label=None
    )

    class Meta:
        model = Rating
        fields = ("star",)
